# Mass-decorrelated-Xbb-tagger
Tools for training and study of mass de-correlated Xbb tagger using deep neural network
by Wei Ding 
wei.ding@cern.ch .

1. reweight
1. process
1. prepare
1. train
1. study


The instructions for how to run are in the file run.sh in each directory.
This tool is inherited from the [tool by Julian Collado Umana](https://gitlab.cern.ch/atlas-boosted-hbb/xbb-tagger-training).
Here is the input MC samples:[https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets).
More studies of this tagger are found [https://indico.cern.ch/event/864911/](https://indico.cern.ch/event/864911/). 

A validation pacakge, [https://gitlab.cern.ch/atlas-boosted-xbb/xbb-validation/](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-validation/) is not at all integrated to this package but has some very useful tools!


# Download files
rucio download the datasets listed here [p3990 v3](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p3990/mc16d-h5-v3-datasets.txt) and original dataset names are [here](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p3990/mc16d-input-datasets.txt)

## Jets
To quickly R&D JZ3W (361023) to JZ8W (361028) are enough.
`mkdir DijetsDatasets` and put the datasets in there (or softlink)

## Higgs
To quickly R&D use the SM boosted Higgs sample 309450.
`mkdir HbbDatasets` and put the datasets in there (or softlink)

# Run Reweight
Go to the directory, from the base dir, `cd reweight`.

Some helper scripts exist 
1. `printOriginalH5.py`: opens and dumps some things from the original h5. Useful for exploring what is available
1. `printINFO.py`: opens and dumps the output files (labeled files).

## Dijet Weights
First run `calculatedDijetsWeights.py` to get the sample weights needed to combine the dijet samples into a smooth distributions accounting for cross-section and filter efficiencies.
```
python calculatedDijetsWeights.py --path path/to/DijetsDatasets/ (include DijetDatasets in the path)
```
Part of the output to the screen is needed in the next script `labelDijetsDatasets.py`. It is the set of numbers in curly brackets `{}` and should be assigned to `listSection`.

## Run Label
There are three, and each is designed to take the same path. Therefore the path should not include the directory with a name reflecting the sample content. For example if the dijet datasets are in `/eos/user/s/student/samples/DijetDatasets/` then the path to use for all these scripts is `/eos/user/s/student/samples/`.

These scripts pull as subset of information from the original h5. It currently using DL1r (not DLrT) and saves quite a few JSS variables.

**To check - if output file exists, causes a seg fault?**

_NOTE: At the time of writing this, it is not immediately clear why this step is needed before the process step._

### Jets
```
python labelDijetsDatasets.py --path path/to/samples
```
The output goes to `../../ReducedDijets/`. The output is quite big, so it would be good to softlink and eos diectory to the output directory or to change the output directory to point to your eos directory.

This is time consuming so best let it run for a while. To run on one DISD use `--dsid 3610XX`. Parallelize by running 1 DSID per shell might be needed. Also, to speed things up, a limited number of h5 files can be used from each dataset, but that has to be consistently done from the start.

Run `plotDijet.py` to check that the dijet weights have been set properly.

# Run Process
Go to the directory, from the base dir, `cd process`.

## Merge Dijets
Go to the directory `cd mergeDijets`.






