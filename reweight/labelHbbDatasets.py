import pandas as pd
import glob
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

hCountKey = "GhostHBosonsCount"
bCountKey = "GhostBHadronsFinalCount"
def label_row(row, isHiggsSample):
    if isHiggsSample:
      if row[hCountKey] >= 1: 
        return "Hbb"
      else:
        return "ignore"
    else :
      if row[hCountKey] >= 1: 
        return "ignore"
      else :
        return "qcd"

filePaths = glob.glob(args.path+"/HbbDatasets/*.h5/*.h5")
list1=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'pt', 'eta', 'GhostBHadronsFinalCount',  'GhostHBosonsCount', 'GhostTQuarksFinalCount', 'mcEventWeight',  'mass', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20','eventNumber']
list2=['DL1r_pu', 'DL1r_pc', 'DL1r_pb', 'relativeDeltaRToVRJet', 'deta', 'dphi', 'dr', 'eta', 'pt']

list3={}
list4={}
list5={}
for i in list2:
  list3[i]=i+"_1"
  list4[i]=i+"_2"
  list5[i]=i+"_3"

count=0
for filePath in filePaths:
  count=count+1
  print "Processing count: ",count
  sourceDataset=filePath.split("/")[-2]
  sourceFile=filePath.split("/")[-1]
  isHiggsSample =True
  h1=pd.read_hdf(filePath, "subjet_VR_1")[list2]
  h1.rename(columns=list3,inplace=True)
  h2=pd.read_hdf(filePath, "subjet_VR_2")[list2]
  h2.rename(columns=list4,inplace=True)
  h3=pd.read_hdf(filePath, "subjet_VR_3")[list2]
  h3.rename(columns=list5,inplace=True)
  newDf =  pd.concat([pd.read_hdf(filePath, "fat_jet")[list1], h1,h2,h3], axis=1)
  newDf["label"] = newDf.apply(lambda row: label_row(row, isHiggsSample), axis=1)
  newDf["pt"] = (newDf["pt"]/1000.0).astype("float64")
  newDf["mass"] = (newDf["mass"]/1000.0).astype("float64")
  newDf["weight"]=newDf["mcEventWeight"]
  newDf=newDf[newDf["label"]!="ignore"]
  newDf["signal"]=1
  newDf["dsid"]=sourceDataset.split("_")[2]
  
  newDfFilePath = "/eos/user/n/ndua/samples/ReducedHbb/" + sourceDataset
  newDf.to_hdf(newDfFilePath, "dataset", format="table", data_columns=True)
  print "Done"







