import pandas as pd
import glob
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
parser.add_argument("--dsid", dest='dsid',  default=int(0), help="dsid")
args = parser.parse_args()

if args.dsid == 0:
    print("Running on all DSIDs")
else: 
    print("Running on DSID: " + str(args.dsid))

# output directory (assuming running from reweight)
outdir="/eos/user/n/ndua/samples/ReducedDijets/"
if not os.path.exists(outdir) and not os.path.islink(outdir):
    print("Outdir does not exist: " + outdir)
    exit()

hCountKey = "GhostHBosonsCount"
bCountKey = "GhostBHadronsFinalCount"
def label_row(row, isDijetSample):
    if isDijetSample:
      if row[hCountKey] >= 1:
        return "ignore"
      else:
        return "qcd"
    else :
      if row[hCountKey] >= 1: 
        return "ignore"
      else :
        return "qcd"

filePaths = glob.glob(args.path+"/DijetsDatasets/*.h5/*.h5")

#list1=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'XbbScoreQCD', 'XbbScoreTop', 'XbbScoreHiggs', 'pt', 'eta', 'GhostHBosonsCount', 'GhostTQuarksFinalCount', 'GhostBHadronsFinalCount',  'mcEventWeight',  'mass', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20']
#list1=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'pt', 'eta', 'GhostBHadronsFinalCount',  'GhostHBosonsCount', 'GhostTQuarksFinalCount', 'mcEventWeight', 'eventNumber', 'mass', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20']
list1=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'pt', 'eta', 'GhostBHadronsFinalCount',  'GhostHBosonsCount', 'GhostTQuarksFinalCount', 'mcEventWeight', 'mass', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20']
#list2=['MV2c10_discriminant', 'IP2D_pb', 'IP2D_pc', 'IP2D_pu', 'IP3D_pb', 'IP3D_pc', 'IP3D_pu', 'JetFitter_N2Tpair', 'JetFitter_dRFlightDir', 'JetFitter_deltaeta', 'JetFitter_deltaphi', 'JetFitter_energyFraction', 'JetFitter_mass', 'JetFitter_massUncorr', 'JetFitter_nSingleTracks', 'JetFitter_nTracksAtVtx', 'JetFitter_nVTX', 'JetFitter_significance3d', 'SV1_L3d', 'SV1_Lxy', 'SV1_N2Tpair', 'SV1_NGTinSvx', 'SV1_deltaR', 'SV1_dstToMatLay', 'SV1_efracsvx', 'SV1_masssvx', 'SV1_pb', 'SV1_pc', 'SV1_pu', 'SV1_significance3d', 'deta', 'dphi', 'dr', 'eta', 'pt', 'rnnip_pb', 'rnnip_pc', 'rnnip_ptau', 'rnnip_pu']
list2=['DL1r_pu', 'DL1r_pc', 'DL1r_pb', 'relativeDeltaRToVRJet', 'deta', 'dphi', 'dr', 'eta', 'pt', 'HadronConeExclTruthLabelID']
list3={}
list4={}
list5={}
for i in list2:
  list3[i]=i+"_1"
  list4[i]=i+"_2"
  list5[i]=i+"_3"

listSection={361024: 7.475289674033148e-08, 361025: 2.3635990589887638e-09, 361026: 2.5826971382978717e-10, 361027: 3.5782314606741578e-12, 361028: 4.2402035200000002e-12, 361029: 0, 361030: 0, 361031: 0, 361032: 0, 361020: 0, 361021: 0, 361022: 0, 361023: 2.419558422857143e-06}

# loop over all files and pick up information of interest down stream
# create a new file with info of fat jet and leading three VR track jets

count=0
for filePath in filePaths:
  count=count+1
  print "Processing count: ",count
  #print(filePath)
  sourceDataset=filePath.split("/")[-2]
  sourceFile=filePath.split("/")[-1]
  dsid=int(sourceDataset.split(".")[2])
  if args.dsid != 0 and int(args.dsid) != dsid:
      print("Skip " + str(dsid))
      continue
  print("\t dataset "+sourceDataset)
  print("\t file    "+sourceFile)
  print("\t DSID    "+str(dsid))
  weight=listSection[dsid]
  print("\tFile from DSID " + str(dsid) + " with weight " + str(weight))
  isDijetSample = True
  print("\t\tRead subjet information")
  ## returns pandas.DataFrame with 1 row per subjet and 1 variable per column
  #print(h1.info())
  hdf = pd.HDFStore(filePath)
  h1  = hdf.get("subjet_VR_1")[list2]
  h1.rename(columns=list3,inplace=True)

  h2  = hdf.get("subjet_VR_2")[list2]
  h2.rename(columns=list4,inplace=True)

  h3  = hdf.get("subjet_VR_3")[list2]
  h3.rename(columns=list5,inplace=True)

  print("\t\tRows: " + str(len(h1)))
  print("\t\tCols: " + str(len(h1.columns)))
  print("\t\tconcat")
  fj = hdf.get("fat_jet")[list1]
  newDf =  pd.concat([fj,h1,h2,h3], axis=1)
  newDf["label"] = newDf.apply(lambda row: label_row(row, isDijetSample), axis=1)
  newDf["pt"] = (newDf["pt"]/1000.0).astype("float64")
  newDf["mass"] = (newDf["mass"]/1000.0).astype("float64")
  newDf=newDf[newDf["label"]!="ignore"]
  newDf["signal"]=0
  newDf["dsid"]=dsid
  print("\t\tadd weight")
  newDf["weight"]=newDf["mcEventWeight"]*weight
  print("\t\twrite")

  #print(newDf)
  newDfFileDir  = outdir + "/" + sourceDataset
  newDfFilePath = newDfFileDir + "/" + sourceFile
  if not os.path.exists(newDfFileDir):
      os.makedirs(newDfFileDir)
  newDf.to_hdf(newDfFilePath, "dataset", format="table", data_columns=True)
  print "Done"







